<?php require('header.php'); ?>

<section class="titre3 titre3Bis" style="background-image: url(img/financement-formation.jpg);">
    <div class="wrap pad-g">
        <div class="contenu">
            <h1>Nos formations sont éligibles
                <span>Seelab vous accompagne dans les démarches.</span>
            </h1>

            <div class="socialBar">
                <ul>
                    <li>
                        <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/SeelabStudio" target="_blank">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>


<div class="content-financement wrap pad-g" id="cpf">
    <div class="vis-a-vis">
        <div class="left-financement">
            <h2>A chacun son financement, en fonction de son statut. Voici le mode d’emploi !</h2>
            <p>Vous avez trouvé la formation de vos rêves : maquettiste PAO, Photoshop, illustrator, InDesign, WordPress ! Oui, mais voilà, comment financer ma formation PAO, Indesign, Illustrator, Photoshop ? Que vous soyez salarié, entrepreneur, étudiant, demandeur d’emploi ou simple particulier, il existe des solutions pour vous aider à financer tout ou partie de votre formation. Vous trouverez dans cette page un résumé des solutions les plus intéressantes.</p>
            <h2>Votre compte CPF</h2>
            <p>l vous suit tout au long de votre carrière, de votre entrée sur le marché du travail jusqu’à votre retraite. Il vous donne droit à 24 heures de formation par an minimum. Il est personnel, et vous pouvez l’utiliser à votre guise. Vous accédez ou créez votre compte à cette adresse : <a href="http://moncompteformation.gouv.fr">http://moncompteformation.gouv.fr</a></p>
        </div>
        <div class="right-financement">
            <img src="img/faux.png" alt="">
        </div>
    </div>
</div>
<div class="content-bis-financement">
    <div class="wrap pad-g">
    <div class="bottom-financement">
        <h2>Peu de personnes profitent des fonds des organismes de financement !</h2>
        <p>Chaque année, tous les salariés et chefs d’entreprise cotisent à des fonds de formation, qu’ils le savent ou non : prélèvements URSSAF ou RSI pour les chefs d’entreprises, cotisations salariales pour les salariés. Ces cotisations ouvrent le droit à des financements pour la formation. Il existe également des solutions pour les entrepreneurs, les indépendants, les patrons des multinationales et les auto-entrepreneurs ! Des enveloppes sont aussi allouées aux demandeurs d’emploi ou aux étudiants. Tous cotisent, souvent sans le savoir.</p>
    </div>
    <div class="triple-colones" id="??">
        <article>
            <div class="imgOffre">
            <i class="icon-Fichier-3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>
            </div>
            <h3>SALARIÉS</h3>
            <p>Pour financer sa formation, le salarié dispose de cinq possibilités, dont le compte personnel de formation (CPF). C’est le dispositif à ce jour le plus efficace. Le CPF concerne tous les salariés et aidera cette année quelque 250 000 bénéficiaires. Il peut ­subventionner jusqu’à 150 heures de formation avec possibilité d’abondement de la part des OPCA. Voir leur site internet.</p>
        </article>
        <article>
            <div class="imgOffre">
            <i class="icon-Fichier-14"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i>
            </div>
            <h3>INDÉPENDANTS</h3>
            <p>Vous êtes indépendant, profession libérale ou chef d’entreprise non salarié ? Comment pouvez-vous faire financer votre formation professionnelle ? Depuis le 1er janvier, le compte personnel de formation (CPF) vous est ouvert également. En conséquence, votre compte sera automatiquement crédité de 24 heures/an. Vous pourrez cumuler ce temps de formation dans la limite de 120 (puis 150) heures, comme les salariés.</p>
        </article>
        <article>
            <div class="imgOffre">
            <i class="icon-Fichier-9"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span></i>
            </div>
            <h3>DEMANDEURS D’EMPLOIS</h3>
            <p>Les demandeurs d’emploi, quant à eux, disposent d’un système spécifique. Les dispositifs de financement des formations et leurs modalités d’attribution étant relativement complexes, je vous conseille de vous adresser directement à votre agence Pôle Emploi.</p>
        </article>
    </div>
    <div class="quatre-colones" id="procedure">
        <h2>Quelle est la procédure à suivre pour obtenir la prise en charge d’une formation ?</h2>
        <div class="content-quatre-colones">
            <section>
            <i class="icon-Fichier-24"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></i>
                <p><strong>Étape 1 : </strong> Je vous adresse la convention de formation + le programme pédagogique.</p>
            </section>
            <section>
            <i class="icon-Fichier-22"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                <p><strong>Étape 2 : </strong> Ces documents sont à adresser à votre OPCA (voir la liste ci-dessus), pour la demande de prise en charge.</p>
            </section>
            <section>
            <i class="icon-Fichier-23"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
                <p><strong>Étape 3 : </strong> vous envoyez les documents justificatifs (que je vous adresse à l’issue de la formation) à l’OPCA : la feuille d’émargement + la facture.</p>
            </section>
            <section>
            <i class="icon-Fichier-21"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span></i>
                <p><strong>Étape 4 : </strong> Vous vous faites rembourser par l’OPCA.</p>
            </section>
        </div>
    </div>
    </div>
</div>



<?php require('footer.php'); ?>