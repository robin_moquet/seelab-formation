<?php require('header.php'); ?>


<section class="contact-map">
    <article class="contact">
        <div>
            <h1>On prend contact ?</h1>
            <p>28 Rue Chef de ville
                <span>17000 LA ROCHELLE</span>
            </p>
            <h4>+33 (0)5 16 19 30 75</h4>
            <h4>elise@seelab.fr</h4>
            <div class="socialBar-contact">
            <ul>
                    <li>
                        <a href="https://www.linkedin.com/company/9485708/">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/seelabstudio/">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/seelab_studio/">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/SeelabStudio">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </article>
    <div class="map" id="map">

    </div>
</section>

<div id="formulaire"></div>
<section class="form-img">
    <div class="form">
        <form action="#">
            <input type="text" placeholder="Nom & Prénom *" class="nom">
            <input type="text" placeholder="Email *" class="email">
            <textarea name="" id="textarea" rows="6"></textarea>
            <div class="validation"><input type="checkbox" name="validation" value="1"><p>En envoyant ce formulaire, j'accepte que les informations transmises soient utilisées dans le cadre de ma demande formulée ci-dessus et de la relation commerciale qui peut en découler. Je prends également connaissance de la politique de confidentialité du site.</p></div>
            <button type="submit">
                <span class="tapez">Tapez votre message</span>
                <span class="envoyer">Envoyer</span>
            </button>

        </form>
    </div>
</section>

<section class="lien-agence">
    <a href="http://www.seelab.fr/">
        <h2>Un doute ? Venez découvrir nos travaux →</h2>
    </a>
</section>


<?php require('footer.php'); ?>