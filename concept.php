<?php require('header.php'); ?>

<div class="titreBg">
<div class="wrap">
    <section class="titre pad-g">
            <h1>Formations personnalisées
                <span>Une approche
                    <strong>TRES</strong> créatif.</span>
            </h1>
            <p>Parce que nous sommes passionnés par notre métier, nous souhaitons transmettre notre savoir faire.</p>
            <div class="socialBar">
                <ul>
                    <li>
                        <a href="https://www.linkedin.com/company/9485708/">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/seelabstudio/">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/seelab_studio/">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/SeelabStudio">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <a href="#slider" class="next js-scrollTo">
            </a>

    </section>
</div>
</div>

<div id="slider"></div>
<div class="slider1 parallax-container">
    <div class="sliderImg parallax">
        <div class="main-carousel">
            <div class="carousel-cell">
            <img class="carousel-cell-image" data-flickity-lazyload="sliderImg/1.jpg" />
                <div class="description">
                    <div class="rond"></div>
                    <h2>Formation sur mesure
                        <span> </span>
                    </h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="carousel-cell">
            <img class="carousel-cell-image" data-flickity-lazyload="sliderImg/2.jpg" />
                <div class="description white">
                    <div class="rond"></div>
                    <h2>Formation sur mesure
                        <span> </span>
                    </h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                </div>
            </div>
            <div class="carousel-cell">
            <img class="carousel-cell-image" data-flickity-lazyload="sliderImg/3.jpg" />
                <div class="description">
                    <div class="rond"></div>
                    <h2>Formation sur mesure
                        <span> </span>
                    </h2>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="nousSommes"></div>
<div class="nousSommes pad-g content">
    <div class="wrap">
        <section>
            <h2>Nous sommes...</h2>
            <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Vitae illo ad quaerat laborum. Quae repellendus tempore sunt? Sequi magni nobis error placeat optio? Sapiente deserunt earum velit debitis animi sed!</p>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quia velit tempora quaerat ipsa. Reprehenderit delectus quisquam dolorum, ipsa in assumenda est debitis iste voluptas dicta rem enim quam, architecto non!</p>
        </section>
    </div>
</div>
<div class="wrap">
    <div class="personnes pad-g">
        <article>
            <div class="imgPerso">
                <img src="" alt="">
            </div>
            <h4>Amandine Roussarie</h4>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sapiente laboriosam cupiditate quibusdam neque doloribus et magnam officiis, dolorum.</p>
            <div class="socialBarPerso">
                <ul>
                    <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href=""><i class="fab fa-instagram"></i></a></li>
                    <li><a href=""><i class="fab fa-twitter"></i></a></li>
                </ul>
            </div>
        </article>
        <article>
            <div class="imgPerso">
                <img src="" alt="">
            </div>
            <h4>Élise Sigoillot</h4>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Sapiente laboriosam cupiditate quibusdam neque doloribus et magnam officiis, dolorum.</p>
            <div class="socialBarPerso">
                <ul>
                    <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href=""><i class="fab fa-instagram"></i></a></li>
                    <li><a href=""><i class="fab fa-twitter"></i></a></li>
                </ul>
            </div>
        </article>
    </div>
</div>

<section class="offres">
    <article>
        <div class="imgOffre">
            <i class="fas fa-globe"></i>
        </div>
        <h4>Notre site web</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis magni minima mollitia nemo impedit in officiis?
            Quos dolore hic vero necessitatibus, earum reprehenderit voluptates ad amet optio. Ipsum, veniam dolorum.</p>
        <div class="bouton">
            <a href="http://www.seelab.fr/">Découvrir</a>
        </div>
    </article>
    <article>
        <div class="imgOffre">
            <i class="fas fa-graduation-cap"></i>
        </div>
        <h4>Nos formations</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis magni minima mollitia nemo impedit in officiis?
            Quos dolore hic vero necessitatibus, earum reprehenderit voluptates ad amet optio. Ipsum, veniam dolorum.</p>
        <div class="bouton">
            <a href="formations.php">En savoir plus</a>
        </div>
    </article>
</section>



<?php require('footer.php'); ?>