<?php require('header.php'); ?>

<div class="titre3" style="background-image: url(img/bureau-seelab.jpg);">
    <div class="wrap pad-g">
        <div class="contenu">
            <h1>Une question ?<span>Besoin d'un renseignement ?</h1>
            <div class="socialBar">
                <ul>
                    <li>
                        <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/SeelabStudio" target="_blank">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="wrap pad-g main-faq">
    <div class="faq">
        <h3>QUESTIONS FRÉQUENTES :</h3>
        <ul>
            <li>
                <div class="titre-faq">
                    <h4>Quels sont les pré-requis pour suivre vos formations ?</h4>
                    <i></i>
                </div>
                <div class="content-faq">
                    <p>Aucun pré-requis, mais une connaissance de l’outil informatique et de son environnement de travail est tout de même recommandée.</p>
                    <div class="nav-faq">
                        <a href="contact.php">NOUS CONTACTER • </a>
                        <div class="socialBar">
                    <ul>
                        <li>
                            <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/SeelabStudio" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="titre-faq">
                    <h4>Formation CPF (compte personnel de formation) : suis-je concerné ?</h4>
                    <i></i>
                </div>
                <div class="content-faq">
                    <p>Une confirmation d’inscription accompagnée de votre convention de formation et du programme du ou des formations sélectionnées, vous seront adressés par e-mail (72 heures maxi), après votre inscription. Si cette formation est prise en charge par un OPCA, vous devez lui transmettre votre convention de formation dès réception.</p>
                    <div class="nav-faq">
                        <a href="contact.php">NOUS CONTACTER • </a>
                        <div class="socialBar">
                    <ul>
                        <li>
                            <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/SeelabStudio" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                    </div>
                </div>
            </li><li>
                <div class="titre-faq">
                    <h4>Formation CPF : quels sont les documents que je receverai suite à mon inscription ?</h4>
                    <i></i>
                </div>
                <div class="content-faq">
                    <p>Le CPF est ouvert à toute personne âgée d’au moins 16 ans, en emploi ou non (article L.6323-1 du Code du travail au 1er janvier 2015). Une dérogation est prévue pour les jeunes de 15 ans en contrat d’apprentissage qui ont déjà effectué leur scolarité du premier cycle de l’enseignement secondaire. Pour les salariés ce dispositif remplace le DIF (Droit Individuel à la Formation). Il sera fermé lorsque la personne est admise à faire valoir l’ensemble de ses droits à la retraite. Voir le site</p>
                    <div class="nav-faq">
                        <a href="contact.php">NOUS CONTACTER • </a>
                        <div class="socialBar">
                    <ul>
                        <li>
                            <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/SeelabStudio" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="titre-faq">
                    <h4>À quel moment devrai-je payer la formation ?</h4>
                    <i></i>
                </div>
                <div class="content-faq">
                    <p>La facture est payable comptant et sans escompte, à l’issue de la formation (si financement OPCA). Si hors financement OPCA : 50 % du montant TTC à la commande. Le solde, à l’issue de votre formation. Dès la réception d’un paiement, vous recevrez une facture acquittée par retour, et les autres documents une fois la formation terminée.</p>
                    <div class="nav-faq">
                        <a href="contact.php">NOUS CONTACTER • </a>
                        <div class="socialBar">
                    <ul>
                        <li>
                            <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/SeelabStudio" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="titre-faq">
                    <h4>À quel moment devrai-je payer la formation ?</h4>
                    <i></i>
                </div>
                <div class="content-faq">
                    <p>La facture est payable comptant et sans escompte, à l’issue de la formation (si financement OPCA). Si hors financement OPCA : 50 % du montant TTC à la commande. Le solde, à l’issue de votre formation. Dès la réception d’un paiement, vous recevrez une facture acquittée par retour, et les autres documents une fois la formation terminée.</p>
                    <div class="nav-faq">
                        <a href="contact.php">NOUS CONTACTER • </a>
                        <div class="socialBar">
                    <ul>
                        <li>
                            <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/SeelabStudio" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="titre-faq">
                    <h4>À quel moment devrai-je payer la formation ?</h4>
                    <i></i>
                </div>
                <div class="content-faq">
                    <p>La facture est payable comptant et sans escompte, à l’issue de la formation (si financement OPCA). Si hors financement OPCA : 50 % du montant TTC à la commande. Le solde, à l’issue de votre formation. Dès la réception d’un paiement, vous recevrez une facture acquittée par retour, et les autres documents une fois la formation terminée.</p>
                    <div class="nav-faq">
                        <a href="contact.php">NOUS CONTACTER • </a>
                        <div class="socialBar">
                    <ul>
                        <li>
                            <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/SeelabStudio" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="titre-faq">
                    <h4>À quel moment devrai-je payer la formation ?</h4>
                    <i></i>
                </div>
                <div class="content-faq">
                    <p>La facture est payable comptant et sans escompte, à l’issue de la formation (si financement OPCA). Si hors financement OPCA : 50 % du montant TTC à la commande. Le solde, à l’issue de votre formation. Dès la réception d’un paiement, vous recevrez une facture acquittée par retour, et les autres documents une fois la formation terminée.</p>
                    <div class="nav-faq">
                        <a href="contact.php">NOUS CONTACTER • </a>
                        <div class="socialBar">
                    <ul>
                        <li>
                            <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/SeelabStudio" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="titre-faq">
                    <h4>À quel moment devrai-je payer la formation ?</h4>
                    <i></i>
                </div>
                <div class="content-faq">
                    <p>La facture est payable comptant et sans escompte, à l’issue de la formation (si financement OPCA). Si hors financement OPCA : 50 % du montant TTC à la commande. Le solde, à l’issue de votre formation. Dès la réception d’un paiement, vous recevrez une facture acquittée par retour, et les autres documents une fois la formation terminée.</p>
                    <div class="nav-faq">
                        <a href="contact.php">NOUS CONTACTER • </a>
                        <div class="socialBar">
                    <ul>
                        <li>
                            <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/SeelabStudio" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="titre-faq">
                    <h4>À quel moment devrai-je payer la formation ?</h4>
                    <i></i>
                </div>
                <div class="content-faq">
                    <p>La facture est payable comptant et sans escompte, à l’issue de la formation (si financement OPCA). Si hors financement OPCA : 50 % du montant TTC à la commande. Le solde, à l’issue de votre formation. Dès la réception d’un paiement, vous recevrez une facture acquittée par retour, et les autres documents une fois la formation terminée.</p>
                    <div class="nav-faq">
                        <a href="contact.php">NOUS CONTACTER • </a>
                        <div class="socialBar">
                    <ul>
                        <li>
                            <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                                <i class="fab fa-linkedin-in"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                        <li>
                            <a href="https://twitter.com/SeelabStudio" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                    </div>
                </div>
            </li>
        </ul>

        <div class="autre-question">
            <h2>Une autres question ?</h2>
            <h3>N'hésitez pas à nous contacter pour en parler.</h3>
            <div class="bouton">
                <a href="contact.php#formulaire">Nous contacter</a>
            </div>
        </div>
    </div>

    <div class="sideBar">
        <h3>Voir aussi ...</h3>
        <ul>
            <li>
                <div class="content-sideBar">
                    <img src="img/ai-img.png" alt="">
                    <h4>Fomration Illustrator</h4>
                    <div class="bouton">
                        <a href="atelier.php">En savoir plus</a>
                    </div>
                </div>
            </li>
            <li>
                <div class="content-sideBar">
                    <img src="img/ai-img.png" alt="">
                    <h4>Fomration Illustrator</h4>
                    <div class="bouton">
                        <a href="atelier.php">En savoir plus</a>
                    </div>
                </div>
            </li><li>
                <div class="content-sideBar">
                    <img src="img/ai-img.png" alt="">
                    <h4>Fomration Illustrator</h4>
                    <div class="bouton">
                        <a href="atelier.php">En savoir plus</a>
                    </div>
                </div>
            </li><li>
                <div class="content-sideBar">
                    <img src="img/ai-img.png" alt="">
                    <h4>Fomration Illustrator</h4>
                    <div class="bouton">
                        <a href="atelier.php">En savoir plus</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>


<?php require('footer.php'); ?>