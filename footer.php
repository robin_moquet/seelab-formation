<footer class="pad-g wrap">
    <div>
        <p><i class="icon-Fichier-11 logo-footer"></i><!--Seelab -->- L'atelier graphique de La Rochalle</p>
        <p>© Design & développement en 2018 par Seelab  |  <a href="#">mentions légales</a></p>
    </div>
    <div class="top">
        <a href="#top" class="js-scrollTo next-top"></a>
    </div>
</footer>

<!--CDN-->
<script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
<script defer src="https://use.fontawesome.com/releases/v5.0.9/js/all.js" integrity="sha384-8iPTk2s/jMVj81dnzb/iFR2sdA7u06vHJyyLlAd4snFpCl/SnyUjRrbdJsw1pGIl" crossorigin="anonymous"></script>
<script src="https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js"></script>
<script src="https://unpkg.com/flickity-fullscreen@1/fullscreen.js"></script>
<script type="text/javascript" src="js/jq-parallax.js"></script>


<!--loader-->
<script>
    $(window).load(function(){
        $('.loader').fadeOut("1000");
    })
</script>

<!--Script-->
<script src="js/main.js"></script>
<script src="js/parallax.js"></script>
<script src="js/resizeImg.js"></script>
<script src="js/couleur.js"></script>

<script src="js/gmaps.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD6ZDatA69MzCPa3m6u6yM0cae3-NS9q0w&callback=initMap" async defer></script>

<!-- <script>
   function verif(textarea) {
    var txt = textarea.value;
    var line = txt.split("\n");
    var nbr_lines = 1;
    for(var i=0;i<line.length;i++) {
        nbr_lines += Math.ceil(line[i].length / (textarea.cols + 7));
    }
    textarea.rows = nbr_lines;
}
</script> -->
</body>

</html>