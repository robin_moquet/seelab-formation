<?php require('header.php'); ?>

<div class="slider1 parallax-container equipe-para">
    <div class="sliderImg parallax">
        <div class="main-carousel">
            
            <div class="carousel-cell">
                <img class="carousel-cell-image" data-flickity-lazyload="sliderImg/2.jpg" />
                <div class="description white">
                    <div class="rond"></div>
                    <h2>Apprenez Photoshop, Indesign ou Illustrator
                        <span> </span>
                    </h2>
                    <p>Pour vous permettre de réaliser enfin seul tous les outils de communication !</p>
                </div>
            </div>
            <div class="carousel-cell">
                <img class="carousel-cell-image" data-flickity-lazyload="sliderImg/3.jpg" />
                <div class="description">
                    <div class="rond"></div>
                    <h2>Maitriser les codes de la mise en forme graphique
                        <span> </span>
                    </h2>
                    <p>Quelque soit votre demande, nous adaptons le contenu de la formation pour vous avec des formations 100% personnalisées</p>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="block-text-vis content">
    <div class="wrap pad-g">
        <div class="titre-left">
            <h3>Notre agence c'est : </h3>
        </div>
        <div class="text-right">
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem sed repellat dolor, a officia iure adipisci magni dolores rerum illum.</p>
            <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Culpa dicta placeat rerum illo quaerat harum, veniam similique odit porro ipsa voluptate rem ducimus neque enim ipsum nobis ab omnis iure.</p>
        </div>
    </div>
</div>

<div class="equipe-bg parallaxBg">
<div class="equipe wrap pad-g" id="studio">
    <article>
        <div class="img-equipe">
            <img src="img/elise-graphiste-seelab.jpg" alt="elise graphiste chez seelab agence de communication">
            <h3>Élise Sigoillot</h3>
            <p>Fondatrice | Grapihste</p>
            <div class="socialBarPerso">
                    <ul>
                        <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href=""><i class="fab fa-instagram"></i></a></li>
                        <li><a href=""><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
                <a href="#" class="btn-plus">EN SAVOIR PLUS</a>
            </div>
            <div class="content-equipe">
                <span class="close-equipe"></span>
                <h4>Formations 1</h4>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas nemo repellendus cum ea? Velit at architecto, adipisci, omnis vitae molestias tempore ab id quasi iste esse impedit dolorum recusandae sint.</p>
                <h4>Parcours</h4>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi consequuntur aspernatur odit modi vero voluptatum architecto, eligendi, praesentium illum excepturi minus debitis repellendus impedit maxime suscipit dolor velit iusto enim?</p>
                <h4>Les plus ...</h4>
                <ol>
                    <li>Element 1</li>
                    <li>Element 2</li>
                    <li>Element 3</li>
                    <li>Element 4</li>
                    <li>Element 5</li>
                </ol>
            </div>
    </article>
    <article>
        <div class="img-equipe">
            <img src="img/amandine-graphiste-seelab.jpg" alt="amandine graphiste chez seelab agence de communication">
            <h3>Amandine Roussarie</h3>
            <p>graphiste bla bla bla...</p>
            <div class="socialBarPerso">
                    <ul>
                        <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href=""><i class="fab fa-instagram"></i></a></li>
                        <li><a href=""><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
                <a href="#" class="btn-plus">EN SAVOIR PLUS</a>
            </div>
            <div class="content-equipe">
            <span class="close-equipe"></span>
                <h4>Formations 2</h4>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas nemo repellendus cum ea? Velit at architecto, adipisci, omnis vitae molestias tempore ab id quasi iste esse impedit dolorum recusandae sint.</p>
                <h4>Parcours</h4>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi consequuntur aspernatur odit modi vero voluptatum architecto, eligendi, praesentium illum excepturi minus debitis repellendus impedit maxime suscipit dolor velit iusto enim?</p>
                <h4>Les plus ...</h4>
                <ol>
                    <li>Element 1</li>
                    <li>Element 2</li>
                    <li>Element 3</li>
                    <li>Element 4</li>
                    <li>Element 5</li>
                </ol>
            </div>
    </article>
    <article>
        <div class="img-equipe">
            <img src="img/amandine-graphiste-seelab.jpg" alt="amandine graphiste chez seelab agence de communication">
            <h3>Amandine Roussarie</h3>
            <p>graphiste bla bla bla...</p>
            <div class="socialBarPerso">
                    <ul>
                        <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href=""><i class="fab fa-instagram"></i></a></li>
                        <li><a href=""><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
                <a href="#" class="btn-plus">EN SAVOIR PLUS</a>
            </div>
            <div class="content-equipe">
            <span class="close-equipe"></span>
                <h4>Formations 2</h4>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas nemo repellendus cum ea? Velit at architecto, adipisci, omnis vitae molestias tempore ab id quasi iste esse impedit dolorum recusandae sint.</p>
                <h4>Parcours</h4>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi consequuntur aspernatur odit modi vero voluptatum architecto, eligendi, praesentium illum excepturi minus debitis repellendus impedit maxime suscipit dolor velit iusto enim?</p>
                <h4>Les plus ...</h4>
                <ol>
                    <li>Element 1</li>
                    <li>Element 2</li>
                    <li>Element 3</li>
                    <li>Element 4</li>
                    <li>Element 5</li>
                </ol>
            </div>
    </article>
    <article>
        <div class="img-equipe">
            <img src="img/amandine-graphiste-seelab.jpg" alt="amandine graphiste chez seelab agence de communication">
            <h3>Amandine Roussarie</h3>
            <p>graphiste bla bla bla...</p>
            <div class="socialBarPerso">
                    <ul>
                        <li><a href=""><i class="fab fa-facebook-f"></i></a></li>
                        <li><a href=""><i class="fab fa-instagram"></i></a></li>
                        <li><a href=""><i class="fab fa-twitter"></i></a></li>
                    </ul>
                </div>
                <a href="#" class="btn-plus">EN SAVOIR PLUS</a>
            </div>
            <div class="content-equipe">
            <span class="close-equipe"></span>
                <h4>Formations 2</h4>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas nemo repellendus cum ea? Velit at architecto, adipisci, omnis vitae molestias tempore ab id quasi iste esse impedit dolorum recusandae sint.</p>
                <h4>Parcours</h4>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sequi consequuntur aspernatur odit modi vero voluptatum architecto, eligendi, praesentium illum excepturi minus debitis repellendus impedit maxime suscipit dolor velit iusto enim?</p>
                <h4>Les plus ...</h4>
                <ol>
                    <li>Element 1</li>
                    <li>Element 2</li>
                    <li>Element 3</li>
                    <li>Element 4</li>
                    <li>Element 5</li>
                </ol>
            </div>
    </article>
</div>
</div>




<?php require('footer.php'); ?>