<?php require('header.php'); ?>

<section class="titreFor" style="background-image: url(img/formation-wp-seelab.png)"">
    <div class="wrap pad-g">
        <h1>Formation <span> Adobe InDesign</span></h1>
        <p>Création de brochure, livre, cartes de visite, flyer, pochettes...</p>
        <img src="img/datadock.png" alt="formation certifié data dock" class="datadock">

        <div class="socialBar">
            <ul>
                <li>
                    <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                        <i class="fab fa-linkedin-in"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                        <i class="fab fa-instagram"></i>
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/SeelabStudio" target="_blank">
                        <i class="fab fa-twitter"></i>
                    </a>
                </li>
            </ul>
        </div>

            
    </div>
</section>


<section class="six-bloc">
    <div class="wrap pad-g">
        <div class="triple-bloc">
            <article>
                <div class="imgFor">
                <i class="icon-Fichier-8 bis"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>
                </div>
                <h3>Pré-requis</h3>
                <p>Quelques bases en informatique. Sensibilité à la communication visuelle.</p>
            </article>
            <article>
                <div class="imgFor">
                <i class="icon-Fichier-10"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                </div>
                <h3>Public concerné</h3>
                <p>Quelques bases en informatique. Sensibilité à la communication visuelle.</p>
            </article>
            <article>
                <div class="imgFor">
                <i class="icon-Fichier-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
                </div>
                <h3>Lieux</h3>
                <p>Quelques bases en informatique. Sensibilité à la communication visuelle.</p>
            </article>
        </div>
        <div class="triple-bloc">
            <article>
                <div class="imgFor">
                <i class="icon-Fichier-4"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>
                </div>
                <h3>A votre disposition</h3>
                <p>Quelques bases en informatique. Sensibilité à la communication visuelle.</p>
            </article>
            <article>
                <div class="imgFor">
                <i class="icon-Fichier-2"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                </div>
                <h3>Le + Seelab</h3>
                <p>Quelques bases en informatique. Sensibilité à la communication visuelle.</p>
            </article>
            <article>
                <div class="imgFor">
                    <i class="fas fa-user-plus"></i>
                </div>
                <h3>Pré-requis</h3>
                <p>Quelques bases en informatique. Sensibilité à la communication visuelle.</p>
            </article>
        </div>
    </div>
</section>



<section class="text-principale">
    <div class="wrap pad-g">
        <div class="corp-first">
            <p class="t-n-2">Formez-vous à l’utilisation d’InDesign, le logiciel de mise en page professionnelle par excellence, grâce à nos
                formations éligibles au CPF.</p>
            <p class="t-n-2">InDesign est un logiciel de la suite Adobe dédié au travail de mise en page de documents. Particulièrement utilisé
                dans les milieu de l’édition il permet de réaliser un travail professionnel de grande qualité.</p>
        </div>

        <div class="corp">
            <div class="text-corp">
                <p>InDesign reste cependant un outil complexe à maîtriser.</p>
                <p>Extrêmement riche en fonctionnalités, son usage variera en fonction de vos besoins. Nos formations s’adaptent
                    ainsi à vos besoins pour vous permettent de monter en compétence sur des points précis :</p>
                <ol>
                    <li>Compréhension de l’environnement de travail</li>
                    <li>Gestion des blocs et des grilles</li>
                    <li>Mise en forme des textes</li>
                    <li>Création de gabarit personnalisés</li>
                    <li>Maîtrise des options d’impression et d’exportation web</li>
                    <li>Export vers/Import depuis les autres outils de la solution Adobe</li>
                    <li>etc</li>
                </ol>
                <div class="bouton">
                    <a href="contact.php#formulaire">Demander un devis</a>
                </div>
            </div>
            <div class="image-text-principale">
                <img src="img/ps-img.jpg" alt="conception web">
            </div>
        </div>

    </div>
</section>

<section class="tarifs">
    <div class="wrap pad-g">
        <!-- <h3>Exemple de tarifs</h3> -->
        <div class="offres-tarifs">
            
            <div>
            <i class="icon-Fichier-19"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span></i>
                <h5>InDesign - Initiation</h5>
                <p>3 jours / 1 100 €</p>
            </div>
            <div>
            <i class="icon-Fichier-18"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span></i>
                <h5>InDesign - Initiation</h5>
                <p>3 jours / 1 100 €</p>
            </div>
            <div>
            <i class="icon-Fichier-17"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span></i>
                <h5>InDesign - Initiation</h5>
                <p>3 jours / 1 100 €</p>
            </div>
        </div>
        <p>Ces tarifs sont indicatifs et sont succeptibles d'évoluer en fonction de votre demande.</p>
    </div>
</section>

<section class="personnalisable" style="background-image: url(img/seelab-bureau4.jpg)">
    <div class="wrap pad-g">
        <article>
        <i class="icon-Fichier-20"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>
            <div class="corps">
                <h4>LE NOMBRE DE JOURS ET LE PROGRAMME PEUVENT ETRE ADAPTÉS</h4>
                <p>Vous connaissez déjà Indesign, vous voulez uniquement developper ou approfondir vos connaissances ? Vous voulez connaitre
                    les nouveautés de la dernière version ou des conseils en création ? Votre budget est limité ?</p>
                <p>
                    Nous nous adaptons et créons pour vous des formations sur mesure. Contactez-nous et nous évaluerons ensemble le temps nécéssaire
                    pour vous former.
                </p>
            </div>
        </article>
    </div>
</section>

<?php require('footer.php'); ?>