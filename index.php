<?php require('header.php'); ?>

<div class="titreBg">
<div class="wrap content">
    <section class="titre pad-g">
        <h1>Vous cherchez une formation hors du commun ?
            <span>Bienvenue chez <!--<img src="img/logo-seelab-mot.png" alt="" class="img-in-h1">-->Seelab, l’atelier graphique
                <strong>TRÉS</strong> créatif.</span>
        </h1>
        <p>Parce que nous sommes passionnés par notre métier, nous souhaitons transmettre notre savoir faire.</p>
        <div class="socialBar">
            <ul>
                <li>
                    <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                        <i class="fab fa-linkedin-in"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                </li>
                <li>
                    <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                        <i class="fab fa-instagram"></i>
                    </a>
                </li>
                <li>
                    <a href="https://twitter.com/SeelabStudio" target="_blank">
                        <i class="fab fa-twitter"></i>
                    </a>
                </li>
            </ul>
        </div>
        <img src="img/datadock.png" alt="formation certifié data dock" class="datadock">
        <a href="#slider" class="next js-scrollTo"></a>

    </section>
</div>
</div><!--titreBg-->

<div id="slider"></div>
<!--Encre-->

<div class="slider1 parallax-container">
    <div class="sliderImg parallax">
        <div class="main-carousel">
            
            <div class="carousel-cell">
                <img class="carousel-cell-image" data-flickity-lazyload="sliderImg/2.jpg" />
                <div class="description white">
                    <div class="rond"></div>
                    <h2>Apprenez Photoshop, Indesign ou Illustrator
                        <span> </span>
                    </h2>
                    <p>Pour vous permettre de réaliser enfin seul tous les outils de communication !</p>
                </div>
            </div>
            <div class="carousel-cell">
                <img class="carousel-cell-image" data-flickity-lazyload="sliderImg/3.jpg" />
                <div class="description">
                    <div class="rond"></div>
                    <h2>Maitriser les codes de la mise en forme graphique
                        <span> </span>
                    </h2>
                    <p>Quelque soit votre demande, nous adaptons le contenu de la formation pour vous avec des formations 100% personnalisées</p>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="formationEnQuatresPoints content">
    <div class="ou">
        <div>
            <img src="img/seelab-bureau4.jpg" alt="seelab centre de formation la rochelle">
        </div>
        <div class="text-ou">
            <article>
                <h3>Où ?</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et nulla nihil ea quam quis totam laborum temporibus
                    iste voluptatum doloremque reprehenderit dolorum earum eum neque, nemo distinctio perferendis, sunt eveniet.</p>
            </article>
            <article>
                <h3>Quand ?</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et nulla nihil ea quam quis totam laborum temporibus
                    iste voluptatum doloremque reprehenderit dolorum earum eum neque, nemo distinctio perferendis, sunt eveniet.</p>
            </article>
            <article>
                <h3>Comment ?</h3>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Et nulla nihil ea quam quis totam laborum temporibus
                    iste voluptatum doloremque reprehenderit dolorum earum eum neque, nemo distinctio perferendis, sunt eveniet.
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Et nulla nihil ea quam quis totam laborum temporibus
                    iste voluptatum doloremque reprehenderit dolorum earum eum neque, nemo distinctio perferendis, sunt eveniet.</p>
                    
            </article>
        </div>
    </div>
</section>

<div class="parallax-container" id="video-acc">
    <div id="video-fond" class="parallax">
        <span id="controle-vid"></span>
        <video src="img/video.mp4" loop controls="true" id="myVideo" muted poster="img/bureau-seelab.jpg"></video>
            <div class="control-video">
                <i class="fas fa-volume-off"></i>
            </div>
    </div>
</div>


<section class="formation content">
    <article>
        <div class="imgOffre">
        <i class="icon-Fichier-3"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>        </div>
        <h4>Vous êtes un professionnel ?</h4>
        <p>Vous êtes chargée de communication ? Assistant(e) de direction ? Ou n’importe quel métier qui nécessite des compétences en maitrise d’images ? Venez compléter vos compétences en apprenant à créer des visuels graphique. Plus besoin de sous-traiter, vous aurez enfin la maitrise de votre développement commercial !</p>
        <div class="bouton">
            <a href="formations.php">En savoir plus</a>
        </div> 
    </article>
    <article>
        <div class="imgOffre">
        <i class="icon-Fichier-14"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i>        </div>
        <h4>Vous êtes un particulier ?</h4>
        <p>Vous êtes passionnés par les images, la photo ou le dessin ? Vous avez essayer de vous dépatouiller de photoshop vous même mais vous avez vite trouver des limites ? Alors l’atelier créatif seelab du vendredi est fait pour vous ! Venez créer et jouer avec vos photos.</p>
        <div class="bouton">
            <a href="atelier.php">En savoir plus</a>
        </div> 
    </article>
    <article>
        <div class="imgOffre">
        <i class="icon-Fichier-9"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span><span class="path18"></span><span class="path19"></span><span class="path20"></span><span class="path21"></span><span class="path22"></span><span class="path23"></span><span class="path24"></span><span class="path25"></span><span class="path26"></span><span class="path27"></span><span class="path28"></span><span class="path29"></span><span class="path30"></span><span class="path31"></span><span class="path32"></span><span class="path33"></span></i>        </div>
        <h4>Vous êtes demandeur d’emploi ?</h4>
        <p>Vous chercher un job et toutes les annonces vous demandent une pluridisciplinarité qui vous fait défaut…? Nous vous apprendrons les bases des logiciels de PAO (Adobe Indesign, Photoshop, Illustrator) afin de pouvoir être plus polyvalents en entreprise.</p>
        <div class="bouton">
            <a href="formations.php">En savoir plus</a>
        </div> 
    </article>
    <article>
        <div class="imgOffre">
        <i class="icon-Fichier-15"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span><span class="path11"></span><span class="path12"></span><span class="path13"></span><span class="path14"></span><span class="path15"></span><span class="path16"></span><span class="path17"></span></i>        </div>
        <h4>Vous êtes un créateur d’entreprise ?</h4>
        <p>Vous venez de vous installer à votre compte et vous aimeriez pouvoir créer vos supports de communication vous-même ? Nous vous aidons pour les créations de différents et supports (flyer, logo, site internet….)</p>
        <div class="bouton">
            <a href="formations.php">En savoir plus</a>
        </div> 
    </article>
</section>

<div id="atelier"></div>
<section class="atelier parallaxBg">
    <div class="left">
        <h2>Le vendredi chez Seelab c’est l’atelier créatif ! </h2>
        <p>Vous souhaitez améliorer vos connaissances dans les logiciels de retouche d’images ? Re-travailler vos photos, les transformer
        et les sublimer ? Faire vous même votre faire part de mariage ou de naissance ?</p> <p>Alors l’atelier seelab est fait pour
        vous !</p><p> Venez en immersion au sein de notre studio de création graphique. Nous vous proposons de venir passer 2h à nos
        cotés et partager nos connaissances en design.</p>
       
        <div class="bouton">
            <a href="atelier.php">En savoir plus</a>
        </div>
    </div>
    <!-- <div class="right">
        <img src="img/atelier-img.svg" alt="atelier">
    </div> -->
</section>

<section class="realiser">
    <div class="real-flex">
        <div class="img-real">
            <img src="img/real-img.jpg" alt="">
        </div>
        <div class="text-real">
            <h2>Réaliser des visuels comme ceux ci.</h2>
            <p>Faites-nous confiance, vous pouvez ! Parceque nos formateurs sont avant tout des designer, ils ont à coeur de
                vous faire réaliser des travaux de qualité. La plupart de nos stagiaires « seelabien » n'avaient jamais
                touché à un logiciel de conception graphique auparavant. Après une formation chez Seelab, vous aurez des
                compétences complémentaires que vous pourrez mettre à profit tout au long de votre carrière professionnelle.
                Venez essayer !</p>
                <div class="bouton">
                    <a href="http://www.seelab.fr/" target="_blank"> Voir le book</a>
                </div>
        </div>
    </div>
</section>


<div id="testimonial"></div>
<section class="temoignages pad-g">
    <div class="slider2">
        <div class="main-carousel">
            <div class="carousel-cell">
                <h6>Formation illustrator</h6>
                <p>"Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis officia porro dolorum nam quasi facere
                    ipsam aperiam? Eligendi, sequi quas vitae, harum, qui sint assumenda veniam dolorum quos quo sunt."</p>
                <h5>Jean-eude Du Bois</h5>
            </div>
            <div class="carousel-cell">
                <h6>L'Atelier</h6>
                <p>"Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis officia porro dolorum nam quasi facere
                    ipsam aperiam? Eligendi, sequi quas vitae, harum, qui sint assumenda veniam dolorum quos quo sunt."</p>
                <h5>Jean-eude Du Bois</h5>
            </div>
            <div class="carousel-cell">
                <h6>Formation photoshop</h6>
                <p>"Lorem ipsum dolor sit amet consectetur adipisicing elit. Debitis officia porro dolorum nam quasi facere
                    ipsam aperiam? Eligendi, sequi quas vitae, harum, qui sint assumenda veniam dolorum quos quo sunt."</p>
                <h5>Jean-eude Du Bois</h5>
            </div>
        </div>
    </div>
</section>

<div class="agence">
    <div class="wrap">
        <h6>Seelab nʼest pas quʼun centre de formation cʼest aussi un studio de création graphique
        </h6>
        <div class="bouton">
            <a href="http://www.seelab.fr/" target="_blank">Voir nos créations</a>
        </div>
    </div>
</div>


<?php require('footer.php'); ?>