<?php require('header.php'); ?>
<div class="parallax-container">
    <div id="video-fond" class="parallax">
        <span id="controle-vid"></span>
        <video src="img/video.mp4" loop controls="true" autoplay muted id="myVideo" poster="img/bureau-seelab.jpg"></video>
        <div class="wrap">
            <div class="control-video">
                <i class="fas fa-volume-off"></i>
                <i class="fas fa-pause"></i>
                
            </div>
        </div>
    </div>
</div>

<section class="formation content">
    <article>
        <div class="imgOffre">
            <!-- <img src="img/SVG/indesign-formation.svg" alt="formation sur indesign"> -->
            <i class="icon-Fichier-5"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></i>
        </div>
        <h4>Formations InDesign</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis magni minima mollitia nemo impedit in officiis?
            Quos dolore hic vero necessitatibus, earum reprehenderit voluptates ad amet optio. Ipsum, veniam dolorum.</p>
        <div class="bouton">
            <a href="indesign.php">En savoir plus</a>
        </div>
    </article>
    <article class="yellow">
        <div class="imgOffre">
            <!-- <img src="img/SVG/photoshop-formation.svg" alt="formation sur photoshop"> -->
            <i class="icon-Fichier-28"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></i>
        </div>
        <h4>Formations Photoshop</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis magni minima mollitia nemo impedit in officiis?
            Quos dolore hic vero necessitatibus, earum reprehenderit voluptates ad amet optio. Ipsum, veniam dolorum.</p>
        <div class="bouton">
            <a href="photoshop.php">En savoir plus</a>
        </div>
    </article>
    <article>
        <div class="imgOffre">
            <!-- <img src="img/SVG/illustrator-formation.svg" alt="formation sur illustrator"> -->
            <i class="icon-Fichier-27"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></i>
        </div>
        <h4>Formations Illustrator</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis magni minima mollitia nemo impedit in officiis?
            Quos dolore hic vero necessitatibus, earum reprehenderit voluptates ad amet optio. Ipsum, veniam dolorum.</p>
        <div class="bouton">
            <a href="illustrator.php">En savoir plus</a>
        </div>
    </article>
    <article>
        <div class="imgOffre">
            <!-- <img src="img/SVG/creation-web.svg" alt="formation sur la creation web"> -->
            <i class="icon-Fichier-6"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span></i>
        </div>
        <h4>Wordpress</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis magni minima mollitia nemo impedit in officiis?
            Quos dolore hic vero necessitatibus, earum reprehenderit voluptates ad amet optio. Ipsum, veniam dolorum.</p>
        <div class="bouton">
            <a href="creaweb.php">En savoir plus</a>
        </div>
    </article>
    <article>
        <div class="imgOffre">
        <i class="icon-Fichier-7"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span></i>
        </div>
        <h4>L'Atelier</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis magni minima mollitia nemo impedit in officiis?
            Quos dolore hic vero necessitatibus, earum reprehenderit voluptates ad amet optio. Ipsum, veniam dolorum.</p>
        <div class="bouton">
            <a href="atelier.php">En savoir plus</a>
        </div>
    </article>
    <article>
        <div class="imgOffre">
        <i class="icon-Fichier-13"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span></i>
        </div>
        <h4>Création graphique</h4>
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Blanditiis magni minima mollitia nemo impedit in officiis?
            Quos dolore hic vero necessitatibus, earum reprehenderit voluptates ad amet optio. Ipsum, veniam dolorum.</p>
        <div class="bouton">
            <a href="faq.php">En savoir plus</a>
        </div>
    </article>
</section>




<?php require('footer.php'); ?>