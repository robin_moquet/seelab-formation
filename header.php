<!DOCTYPE html>
<html>
<head>
    <title>Seelab formation - La Rochelle</title>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">

    <meta name="viewport" content="width=device-width; initial-scale=1.0">

    <!--LINK-->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/mediaqueries.css">

    <!--SLIDER CDN-->
    <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css">
    <link rel="stylesheet" href="https://unpkg.com/flickity-fullscreen@1/fullscreen.css">

    <!--Icon ios & favicon-->
    <link rel="apple-touch-icon" href="img/touch-icon-iphone.png" />
    <link rel="icon" type="image/png" href="img/favicon.png" />
    <link rel="stylesheet" href="css/styleIcon.css">

</head>
<body>
<div class="reseauxTop">
    <div class="links">
            <div class="facebook">
                <a href="https://www.facebook.com/seelabstudio/" target="_blank"><i class="fab fa-facebook-f"></i></a>
            </div>
            <div class="twitter">
                <a href="https://twitter.com/SeelabStudio" target="_blank"><i class="fab fa-twitter"></i></a>
            </div>
            <div class="linkedin">
                <a href="https://www.linkedin.com/company/9485708/" target="_blank"><i class="fab fa-linkedin-in"></i></a>
            </div>
            <div class="instagram">
                <a href="https://www.instagram.com/seelab_studio/" target="_blank"><i class="fab fa-instagram"></i></a>
            </div>
        </div>
        <div class="close-reseauxTop"></div>
</div>

    <header class="pad-g">
        <!--***********MENU**************-->
        <div class="loader">
            <img src="img/loader.svg" alt="">
        </div>

        <div id="top"></div>

        <div class="navTop wrap pad-g">
                <div class="logo-navTop">
                    <a href="index.php"><i class="icon-Fichier-12"></i></a> 
                </div>
                <div class="navControls-navTop">
                    <ul>
                        <li><a href="">MENU</a></li>
                        <li><a href="" class="lienReseauxTop"><i class="icon-Fichier-16"></i></a></li>
                    </ul>

                </div>
        </div>

        <div class="menuOverlay">
        <i class="before"></i>
        
            <header>
                <p><strong>Seelab Formation </strong> L'atelier graphique de La Rochelle</p> 
            </header>

            <div class="croix"></div>

            <div class="bodyMenuOverlay">
                
                <ul>
                    <li class="menuItem">
                        <a href="index.php">Accueil</a>

                       
                    </li>
                    
                    <li class="menuItem">
                        <a href="formations.php">Nos formations</a>

                        <ul class="subMenu">
                            <li><a href="indesign.php">InDesign</a></li>
                            <li><a href="illustrator.php">Illustrator</a></li>
                            <li><a href="photoshop.php">Photoshop</a></li>
                            <li><a href="creaweb.php">Wordpress</a></li>
                            <li><a href="creagraphique.php">Création graphique</a></li>
                        </ul>
                    </li>
                    <li class="menuItem">
                        <a href="ateliers.php">Les Ateliers</a>

                        <ul class="subMenu"> 
                            <li><a href="atelier.php">L'atelier créatif</a></li>
                            <li><a href="workshop.php">Workshop</a></li>
                            <li><a href="cours-particulier.php">Cours particulier</a></li>
                        </ul>
                    </li>
                    <li class="menuItem">
                        <a href="equipe.php">Qui sommes nous</a>
                        
                    </li>
                    <li class="menuItem">
                        <a href="financement.php">Financement</a>
                        
                    </li>
                    <li class="menuItem">
                        <a href="faq.php">FAQ</a>
                    </li>
                    <li class="menuItem">
                        <a href="contact.php">Contact</a>
                    </li>
                </ul>

            </div>

            <footer>
                <div class="social">
                    <p>Sur les réseaux </p>
                        <div class="socialBar">
                            <ul>
                                <li><a href="https://www.linkedin.com/company/9485708/" target="_blank"><i class="fab fa-linkedin-in"></i></a></li>
                                <li><a href="https://www.facebook.com/seelabstudio/" target="_blank"><i class="fab fa-facebook-f"></i></a></li>
                                <li><a href="https://www.instagram.com/seelab_studio/" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="https://twitter.com/SeelabStudio" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            </ul>
                            
                        </div>
                </div>
            </footer>

        </div>

        <div class="menuNav pad-g">
            <div class="logo-menuNav">
                <a href="index.php">
                <i class="icon-Fichier-11"></i>
                    <!-- <h6>Seelab formation</h6> -->
                </a>
            </div>
            <div class="ul-menuNav">
            <ul class="item-menuNav">
                    <li class="menuItem">
                        <a href="index.php">Accueil</a>

                        
                    </li>
                    
                    <li class="menuItem">
                        <a href="formations.php">Nos formations</a>

                        <ul class="subMenu">
                            <li><a href="indesign.php">InDesign</a></li>
                            <li><a href="illustrator.php">Illustrator</a></li>
                            <li><a href="photoshop.php">Photoshop</a></li>
                            <li><a href="creaweb.php">Wordpress</a></li>
                            <li><a href="creagraphique.php">Création graphique</a></li>
                        </ul>
                    </li>
                    <li class="menuItem">
                        <a href="ateliers.php">Les Ateliers</a>

                        <ul class="subMenu"> 
                            <li><a href="atelier.php">L'atelier créatif</a></li>
                            <li><a href="workshop.php">Workshop</a></li>
                            <li><a href="cours-particulier.php">Cours particulier</a></li>
                        </ul>
                    </li>
                    <li class="menuItem">
                        <a href="equipe.php">Qui sommes nous</a>
                    </li>
                    <li class="menuItem">
                        <a href="financement.php">Financement</a>

                       
                    </li>
                    <li class="menuItem">
                        <a href="faq.php">FAQ</a>
                    </li>
                    <li class="menuItem">
                        <a href="contact.php">Contact</a>
                    </li>
                    <li><a href="" class="lienReseauxTop"><i class="icon-Fichier-16"></i></a></li>
                </ul>
            </div>
            <div class="btnMenuOverlay-menuNav">
                <a href="">MENU</a>
            </div>
        </div>

        
    </header>
<!--****************FinHeader*************-->