<?php require('header.php'); ?>

<div class="titre3 titre3Bis" style="background-image: url(img/financement-formation.jpg);">
    <div class="wrap pad-g">
        <div class="contenu">
            <h1>Une question ?<span>Besoin d'un renseignement ?</h1>
            <div class="socialBar">
                <ul>
                    <li>
                        <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/SeelabStudio" target="_blank">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="btn-fixe-resa">
    <a href="#">
        <img src="img/calendrier.png" alt="calendrier">
        <h3>RÉSERVER</h3>
    </a>
</div>


<div class="bg-blue">
    <div class="wrap pad-g">
        <p>Vous souhaitez améliorer vos connaissances dans les logiciels de retouche dʼimages ?</p>
        <p>Re-travailler vos photos, les transformer et les sublimer ?</p>
        <p>Faire vous même votre faire part de mariage ou de naissance ?</p>
        <p>Alors lʼàtelier seelab est fait pour vous !</p>
        <p>Venez en immersion au sein de notre studio de création graphique. Nous vous proposons de venir passer 3h à nos cotés
            et partager nos connaissances en design.
        </p>

    </div>
</div>

<div class="bonnes-rai">
    <h3 class="bonnes-rai-h">
        3 bonnes raisons de participer à<strong>l'àtelier graphique seelab</strong>
    </h3>

    <div class="flex-trois">
        <article>
            <h2>1</h2>
            <h6>do it yourself</h6>
        </article>
        <article>
            <h2>2</h2>
            <h6>Rendu de qualité</h6>
        </article>
        <article>
            <h2>3</h2>
            <h6>convivialité</h6>
        </article>
    </div>
</div>
<div class=" wrap pad-g">
    <div class="bloc1">
        <p>Atelier animé par Elise & Amandine, graphistes chez Seelab. Nous vous aidons sur vos choix créatifs et nous vous donnons des conseils techniques. Le tout dans une atmosphére conviviale et très créative.</p>
    </div>
</div>

<div class="wrap pad-g">
    <h3>Comment se déroule l'àtelier ?</h3>
    <div class="p-20">
        <p>De 14 à 15h30 nous vous expliquons une technique précise sur un logiciel.</p>
        <p>De 15h30 à 17h vos travaillez vous-même sur vos propres images.</p>
    </div>
</div>

    <section class="ateliers wrap pad-g">
        <div class="triple-bloc">
                <article>
                    <div class="imgFor">
                    <i class="icon-Fichier-8 bis"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>
                    </div>
                    <h3>Prè requis</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut excepturi tenetur molestias sed fugiat.</p>
                </article>
                <article>
                    <div class="imgFor">
                    <i class="icon-Fichier-10"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                    </div>
                    <h3>À votre disposition</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut excepturi tenetur molestias sed fugiat.</p>

                </article>
                <article>
                    <div class="imgFor">
                    <i class="icon-Fichier-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
                    </div>
                    <h3>Le + seelab</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut excepturi tenetur molestias sed fugiat.</p>
                </article>
        </div>
    </section>

<div class="wrap pad-g">
    <h3>Inscription trimestrielle</h3>
    <div class="p-20">
        <p>(3 ateliers) 180€</p>
        <p>Inscription annuelle 10 atliers : 520.00€</p>
        <p>Engagement après 1 cours dʼessai.</p>
        <p>En cas dʼabsence ou dʼabandon, les séances non suivies ne seront remboursées quʼen cas de force majeure de droit commun.</p>
        <p>Nous avons déjà hate de vous rencontrer :-)</p>
        <p>*Si un autre horaire vous conviendrez mieux, nʼhésitez pas à nous contacter !</p>
    </div>
</div>

<br/>
<br/>
<br/>
<br/>

<section class="lien-agence">
    <a href="contact.php#formlaire">
        <h2>Intéressé ? Contactez-nous →</h2>
    </a>
</section>




<?php require('footer.php'); ?>