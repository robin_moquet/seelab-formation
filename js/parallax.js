$(document).ready(function () {
    var parallaxElements = $('.parallax'),
        parallaxQuantity = parallaxElements.length;

    let mobile = false;
    let desktop = false;

    sizeScreen();

    function sizeScreen() {
        let screen = $(window).width();
        if (screen > 1023) {
            desktop = true;
            mobile = false;
        } else {
            mobile = true;
            desktop = false;
        }
    }

    $(window).resize(function () {
        if ($(window).width() > 1023) {
            desktop = true;
            mobile = false;
        } else {
            mobile = true;
            desktop = false;
        }
    })

        if(desktop){
            $(window).on('scroll', function () {

                window.requestAnimationFrame(function () {

                    for (var i = 0; i < parallaxQuantity; i++) {
                        var currentElement = parallaxElements.eq(i);
                        var scrolled = $(window).scrollTop();

                        currentElement.css({
                            'transform': 'translate3d(0,' + scrolled * 0.25 + 'px, 0)'
                        });

                    }
                });

            });
        }//if

});