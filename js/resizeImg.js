function redimensionnement() {


    let mobile = false;
    let desktop = false;

    sizeScreen();

    function sizeScreen() {
        let screen = $(window).width();
        if (screen > 1023) {
            desktop = true;
            mobile = false;
        } else {
            mobile = true;
            desktop = false;
        }
    }

    $(window).resize(function () {
        if ($(window).width() > 1023) {
            desktop = true;
            mobile = false;
        } else {
            mobile = true;
            desktop = false;
        }
    })


    var $image = $('.slider1 .carousel-cell-image');
    var image_width = $image.width();
    var image_height = $image.height();

    var over = image_width / image_height;
    var under = image_height / image_width;

    var body_width = $(window).width();

    if (desktop) {
        var body_height = $(window).height();
    } else {
        var body_height = ($(window).height()) * 0.8;
    }

    if (body_width / body_height >= over) {
        $image.css({
            'width': body_width + 'px',
            'height': Math.ceil(under * body_width) + 'px',
            'left': '0px',
            'top': Math.abs((under * body_width) - body_height) / -2 + 'px'
        });
    } else {
        $image.css({
            'width': Math.ceil(over * body_height) + 'px',
            'height': body_height + 'px',
            'top': '0px',
            'left': Math.abs((over * body_height) - body_width) / -2 + 'px'
        });
    }
}

$(document).ready(function () {

    // Au chargement initial  
    console.log('resizeImg');
    redimensionnement();

    setInterval(function(){
        redimensionnement();
    }, 5000);

    // En cas de redimensionnement de la fenêtre
    $(window).resize(function () {
        redimensionnement();
    });

});