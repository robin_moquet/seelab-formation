$(document).ready(function () {
    
    gestionCouleurs();

    function gestionCouleurs(){
        let page = window.location.pathname;
        if(page.search('/indesign.php') !== -1){
            $('body').addClass('indesign');
        }//if indesign
        if(page.search('/illustrator.php') !== -1){
            $('body').addClass('illustrator');
        }//if illustrator
        if(page.search('/photoshop.php') !== -1){
            $('body').addClass('photoshop');
        }//if photoshop
        if(page.search('/creaweb.php') !== -1){
            $('body').addClass('wp');
        }//if wp
        if(page.search('/faq.php') !== -1){
            $('body').addClass('faq');
        }//if wp
    }//gestionCouleurs

});//doc.ready