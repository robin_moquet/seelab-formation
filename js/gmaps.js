
      function initMap() {
        // Styles a map in night mode.

        let page = window.location.pathname;
        if(page.search('/contact.php') !== -1){

          var myLatLng = {lat: 46.185443, lng: -1.246460};
          var latMarquer = {lat: 46.1585422, lng: -1.155712699999981}

          var map = new google.maps.Map(document.getElementById('map'), {
            center: myLatLng,
            zoom: 10,
            fullscreenControl: false,
            mapTypeControl: false,
            scrollwheel: false,
            styles: [
              {
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#f5f5f5"
                  }
                ]
              },
              {
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#616161"
                  }
                ]
              },
              {
                "elementType": "labels.text.stroke",
                "stylers": [
                  {
                    "color": "#f5f5f5"
                  }
                ]
              },
              {
                "featureType": "administrative",
                "elementType": "geometry",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "administrative.land_parcel",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#bdbdbd"
                  }
                ]
              },
              {
                "featureType": "poi",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "poi",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#eeeeee"
                  }
                ]
              },
              {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#757575"
                  }
                ]
              },
              {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#e5e5e5"
                  }
                ]
              },
              {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#9e9e9e"
                  }
                ]
              },
              {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#ffffff"
                  }
                ]
              },
              {
                "featureType": "road",
                "elementType": "labels.icon",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "road.arterial",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#757575"
                  }
                ]
              },
              {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#dadada"
                  }
                ]
              },
              {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#616161"
                  }
                ]
              },
              {
                "featureType": "road.local",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#9e9e9e"
                  }
                ]
              },
              {
                "featureType": "transit",
                "stylers": [
                  {
                    "visibility": "off"
                  }
                ]
              },
              {
                "featureType": "transit.line",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#e5e5e5"
                  }
                ]
              },
              {
                "featureType": "transit.station",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#eeeeee"
                  }
                ]
              },
              {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [
                  {
                    "color": "#4ff4ff"
                  }
                ]
              },
              {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [
                  {
                    "color": "#4ff4ff"
                  }
                ]
              }
            ]
          });

          var image = 'img/marqueur4.png'

          var contentString = '<div class="infoBulle-map">' +
          '<div class="infos-map">' +
            '<h6>Seelab formation</h6>' +
            '<p>28 Rue Chef de ville</p>' +
            '<p> 17000 LA ROCHELLE</p>' +
          '</div>' +
          '<div class="bouton-map">' +
            '<div class="inti-map">' +
              '<a href="https://www.google.fr/maps/dir//SEELAB+STUDIO,+28+Rue+Chef+de+ville,+17000+La+Rochelle/@46.1585457,-1.1579017,17z/data=!4m8!4m7!1m0!1m5!1m1!1s0x4801537381c5a3dd:0xd72ff2222eadf318!2m2!1d-1.155713!2d46.158542">Itinéraire</a>' +
            '</div>' +
            '<div class="contact-map">' +
              '<a href="#formulaire" class="js-scrollTo">Contact</a>' +
            '</div>' +
          '</div>' +

          '</div>';//contentString

          var infowindow = new google.maps.InfoWindow({
            content: contentString
          });

          var marker = new google.maps.Marker({
              position: latMarquer,
              map: map,
              title: 'Seelab formation',
              icon: image
            });

            marker.addListener('click', function() {
              infowindow.open(map, marker);
            });

          }//if


      }//initMap


