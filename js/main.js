$(document).ready(function () {


    $('.js-scrollTo').click(function () {
        var the_id = $(this).attr("href");
        //console.log('test');
        $('html, body').animate({
            scrollTop: $(the_id).offset().top
        }, 'slow');
        return false;
    });


/****************************************************************************************************
                                            Resize
****************************************************************************************************/

let mobile = false;
let desktop = false;

sizeScreen();
function sizeScreen(){
    let screen = $(window).width();
    if(screen > 1023){
        desktop  = true;
        mobile = false;
    } else{
        mobile = true;
        desktop = false;
    }
}

$(window).resize(function(){
    if($(window).width() > 1023){
        desktop = true;
        mobile = false;
    } else{
        mobile = true;
        desktop = false;
    }
})


$(window).resize(function(){
    if(desktop){
        location.reload();
    }  
})


/****************************************************************************************************
                                            Menu
****************************************************************************************************/

let reseauxTopOpen = false;

$('.reseauxTop .close-reseauxTop').hide();

$('body').click(function(){
    if(reseauxTopOpen){
        reseauxTopOpen = false;
        $('.reseauxTop .close-reseauxTop').hide();
        $('.reseauxTop .links').animate({top: -300}, 500);
        $('.close-reseauxTop').css("cursor", "auto");
    }
})

$('.lienReseauxTop').click(function(e){
    e.preventDefault();
    $('.reseauxTop .links').animate({top: 0}, 500);
    $('.reseauxTop .close-reseauxTop').show();
    $('.close-reseauxTop').css("cursor", "url(img/cursorClose.png), auto");
    setTimeout(function(){
        reseauxTopOpen = true;
    }, 1);
})


$('.menuOverlay').hide();
$('.menuOverlay .bodyMenuOverlay').hide();
$('.menuOverlay header').hide();
$('.menuOverlay footer').hide();

if(desktop){
    $('.menuOverlay').click(function(){
            $('.menuOverlay .bodyMenuOverlay').fadeOut(150);
            $('.menuOverlay header').fadeOut(150);
            $('.menuOverlay footer').fadeOut(150);
            $('.menuOverlay header').css('margin-left', '-50px');
            $('.menuOverlay footer').css('margin-right', '-50px');
            $('.menuOverlay .bodyMenuOverlay').css('transform', 'scale(1.2)');
        setTimeout(function(){
            $('.menuOverlay').fadeOut();
            $('.menuOverlay .before').css('border-width', '0px');
            
        }, 100);
    })

    $('.menuOverlay li a').click(function(){
        $('.menuOverlay .bodyMenuOverlay').fadeOut(150);
        $('.menuOverlay header').fadeOut(150);
        $('.menuOverlay footer').fadeOut(150);
        $('.menuOverlay header').css('margin-left', '-50px');
        $('.menuOverlay footer').css('margin-right', '-50px');
        $('.menuOverlay .bodyMenuOverlay').css('transform', 'scale(1.2)');
    setTimeout(function(){
        $('.menuOverlay').fadeOut();
        $('.menuOverlay .before').css('border-width', '0px');
        
    }, 100);
})

    $('.navTop .navControls-navTop ul li:eq(0) a').click(function(e){
        e.preventDefault();
        $('.menuOverlay').fadeIn(150);
        $('.menuOverlay .before').css('border-width', '500px 900px');
        
        setTimeout(function(){
            $('.menuOverlay .bodyMenuOverlay').fadeIn(100);
            $('.menuOverlay header').fadeIn(100);
            $('.menuOverlay footer').fadeIn(100);
            $('.menuOverlay header').css('margin-left', '0px');
            $('.menuOverlay footer').css('margin-right', '0px');
            $('.menuOverlay .bodyMenuOverlay').css('transform', 'scale(1)');
            
        }, 300);
        
    })

    $('.menuOverlay .bodyMenuOverlay ul li.menuItem').hover(affichageSubMenu, removeSubMenu);
    $('.menuNav .ul-menuNav ul li.menuItem').hover(affichageSubMenu, removeSubMenu);

    function affichageSubMenu(){
        let ul = $(this).find('ul.subMenu');
        let liDuHover = $(this);
        ul.css("opacity", "1");
        let li = $(this).find('ul.subMenu li');
        for(let i = 1; i <= li.length; i++){
            let delay = i + '00';
            setTimeout(function(){
                let a = liDuHover.find('ul.subMenu li:nth-of-type(' + i + ') a');
                a.css("top", "0");
            }, delay);
        }
    }

    function removeSubMenu(){
        let ul = $(this).find('ul.subMenu');
        let liDuHover = $(this);
        ul.css("opacity", "0");
        let li = $(this).find('ul.subMenu li');
        for(let i = 1; i <= li.length; i++){
            let delay = i + '00';
            setTimeout(function(){
                let a = liDuHover.find('ul.subMenu li:nth-of-type(' + i + ') a');
                a.css("top", "23px");
            }, delay);
        }
    }

}//if desktop

var previousScroll = 0;
    var minimaScroll = 300;

    $(window).scroll(function(){
        
        var currentScroll = $(this).scrollTop();

        if(currentScroll > 0 && currentScroll < $(document).height() - $(window).height()){

            if(currentScroll > previousScroll || currentScroll < minimaScroll){
                $('.menuNav').css("top", "-300px");
            } else if(currentScroll < previousScroll && currentScroll > minimaScroll){
                $('.menuNav').css("top", "0px");
            }
            previousScroll = currentScroll;
        }
    })

    $('.menuNav ul li').click(function(){
        $('.menuNav').css("top", "-300px");
    })

if(mobile){
    $('.navTop .navControls-navTop ul li:eq(0) a').click(function(e){
        e.preventDefault();
        $('.menuOverlay').show();
        $('.menuOverlay').animate({right: '0%'}, 300);
        $('.menuOverlay .bodyMenuOverlay').show();
        $('.menuOverlay header').show();
        $('.menuOverlay footer').show();
    })

    $('.menuNav .btnMenuOverlay-menuNav a').click(function(e){
        e.preventDefault();
        $('.menuOverlay').show();
        $('.menuOverlay').animate({right: '0%'}, 300);
        $('.menuNav').css("top", "-300px");
        $('.menuOverlay .bodyMenuOverlay').show();
        $('.menuOverlay header').show();
        $('.menuOverlay footer').show();
    })

    $('.menuOverlay .croix').click(function(){
        //$('.menuOverlay').hide();
        $('.menuOverlay').animate({right: '-100%'}, 300);
    })

    $('.menuOverlay .bodyMenuOverlay ul li').click(function(){
        //$('.menuOverlay').hide();
        $('.menuOverlay').animate({right: '-100%'}, 300);
    })
}//if mobile



/****************************************************************************************************
                                            Parallax BG
****************************************************************************************************/
if(desktop){
    $('.parallaxBg').parallax("center", 0.5, true);
}


/****************************************************************************************************
                                            Slider
****************************************************************************************************/

$('.slider1 .sliderImg .main-carousel').flickity({
    // options
    cellAlign: 'center',
    contain: true,
    autoPlay: 5000,
    fullscreen: false,
    pageDots: false,
    pauseAutoPlayOnHover: false,
    adaptiveHeight: true,
    imagesLoaded: true,
    lazyLoad: true,
  });

//   var $carousel = $('.carousel').flickity()
// //   .flickity('next')
// //   .flickity( 'select', 4 );
// //   $('.button--next-wrapped').on( 'click', function() {
// //     $carousel.flickity( 'next', true );
// //   });

// var cellElements = $carousel.flickity('getCellElements');
// console.log(cellElements);

// var flkty = $('.carousel').data('flickity')
// // access Flickity properties
// console.log( 'carousel at ' + flkty.selectedIndex )

//   test();
//   function test(){
//       console.log('test');
//       $('button').click(function(){
//           console.log('test test');
//       })
//   }

//   $('.main-carousel').on("click", "img", function(e){
//       //e.preventDefault();
//       console.log('blabla');
//   });

//   $('.slider1 button').click(function(){
//     let lastDiv = $('.flickity-slider').last();
//     console.log(lastDiv);
//     console.log('cocou');
//   });//click on the next btn on last div


  /***********ANIMATION DECRIPTION**************** */
  let timeOutAnimation;
  
  animationSlider();
  function animationSlider(){
    $('.slider1 .main-carousel .description .rond').animate({top: 20}, 0);
    $('.slider1 .main-carousel .description h2 span').animate({width: 0}, 0);
    $('.slider1 .main-carousel .description .rond').animate({top: 0}, 100);
    if(desktop){
        $('.slider1 .main-carousel .description h2 span').animate({width: 100}, 5000);
    } else{
        $('.slider1 .main-carousel .description h2 span').animate({width: 60}, 5000);
    }
    timeOutAnimation = setTimeout(animationSlider, 5000);
  }

  $('.slider1 button').click(function(){
      clearTimeout(timeOutAnimation);
      $('.slider1 .main-carousel .description h2 span').stop();
    $('.slider1 .main-carousel .description h2 span').animate({width: 0}, 1);
  })

  $('.slider1').click(function(){
    clearTimeout(timeOutAnimation);
    $('.slider1 .main-carousel .description h2 span').stop();
  $('.slider1 .main-carousel .description h2 span').animate({width: 0}, 1);
})

  $('.slider1').bind('touchmove', function(){
    clearTimeout(timeOutAnimation);
    $('.slider1 .main-carousel .description h2 span').stop();
  $('.slider1 .main-carousel .description h2 span').animate({width: 0}, 1);
})


  $('.slider2 .main-carousel').flickity({
    // options
    cellAlign: 'center',
    contain: true,
    autoPlay: 5000,
    fullscreen: false,
  });




  /**************************************VIDEO****************************** */
  
video();
function video(){
    let page = window.location.pathname;
    if(desktop && page.search('/formations.php') !== -1){
        var vid = document.getElementById('myVideo');
        vid.autoplay = true;
        vid.controls = false;
    }
    if(desktop && page.search('/index.php') !== -1 || page == '/rmoquet/seelab-formation/'){
        var vid = document.getElementById('myVideo');
        //vid.autoplay = true;
        vid.controls = false;
        $('#video-fond').css('top', '-71vh');
    }
    if(mobile && page.search('/formations.php') !== -1){
        var vid = document.getElementById('myVideo');
        vid.autoplay = false;
        vid.controls = false;

        $('#controle-vid').click(function(){
            vid.play();
            $(this).hide();
        });
    }
    if(mobile && page.search('/index.php') !== -1 || page == '/rmoquet/seelab-formation/'){
        var vid = document.getElementById('myVideo');
        vid.autoplay = false;
        vid.controls = false;
        $('#video-fond').css('top', '0');
        $('#controle-vid').click(function(){
            vid.play();
            $(this).hide();
        });
    }
}

let firstVideo = true;

$(window).scroll(function(){
    let page = window.location.pathname;
    if(page.search('/index.php') !== -1 || page == '/rmoquet/seelab-formation/' && desktop){
        let scrollToTop = $(this).scrollTop();
        let posTop = document.getElementById('video-acc').offsetTop;
        let windHei = $(window).height();
        //console.log(scrollToTop);
        if(desktop){

        if(scrollToTop > posTop - windHei && scrollToTop < posTop + windHei && firstVideo){
            var vid = document.getElementById('myVideo');
            vid.play();
            //console.log('coucou');
            //firstVideo = false;
        } else{
            var vid = document.getElementById('myVideo');
            vid.pause();
        }
        }
    }//if page
})

/***********CONTROLS******************** */

$('.control-video').on("click", ".fa-volume-off", function(){
    var vid = document.getElementById('myVideo');
    vid.muted = false;
    $(this).removeClass('fa-volume-off');
    $(this).addClass('fa-volume-up');
}); // volume

$('.control-video').on("click", ".fa-volume-up", function(){
    var vid = document.getElementById('myVideo');
    vid.muted = true;
    $(this).removeClass('fa-volume-up');
    $(this).addClass('fa-volume-off');
}); // volume

$('.control-video').on("click", ".fa-pause",function(){
    var vid = document.getElementById('myVideo');
    let play = true;
    if(play){
        vid.pause();
        play = false;
    }
    $(this).removeClass('fa-pause');
    $(this).addClass('fa-play');
});//pause

$('.control-video').on("click", ".fa-play",function(){
    var vid = document.getElementById('myVideo');
    let play = false;
    if(!play){
        vid.play();
        play = true;
    }
    $(this).removeClass('fa-play');
    $(this).addClass('fa-pause');
});//pause



/*******************************************FAQ**************************************** */

$('.main-faq .faq .titre-faq').click(function(){
    console.log('click');
    let li = $(this).parents('li');
    let liclass = li.attr('class');
    if(liclass == 'open'){
        li.removeClass('open');
    } else{
        $('.faq ul li').removeClass('open');
        li.toggleClass('open');
    }
    
})


/*******************************************EQUIPE**************************************** */

$('.equipe .img-equipe .btn-plus').click(function(e){
    e.preventDefault();
    let article = $(this).parents('article');
    let classArticle = article.attr('class');
    article.find('.content-equipe h4').css("transition", "all 0.3s ease 0.5s");
    article.find('.content-equipe p').css("transition", "all 0.3s ease 0.7s");
    article.find('.content-equipe ol').css("transition", "all 0.3s ease 0.8s");
    if(classArticle != 'open-equipe'){
        article.addClass('open-equipe');
        openDesc();
        function openDesc(){
            let desc = article.find('.content-equipe');
            if(desktop){
                desc.animate({
                    width: "100%",
                    padding: "7vw"
                }, 500);
            } else if(mobile){
                desc.animate({
                    width: "100%",
                    padding: "2vw"
                }, 500);
            }
        }
        
    } else{
        article.removeClass('open-equipe');
        article.find('.content-equipe h4').css("transition", "all 0.3s ease 0s");
        article.find('.content-equipe p').css("transition", "all 0.3s ease 0.2s");
        article.find('.content-equipe ol').css("transition", "all 0.3s ease 0.3s");
        closeDesc();
        function closeDesc(){
            let desc = article.find('.content-equipe');
            desc.delay(700).animate({
                width: "0%",
                padding: "0vw"
            }, 500);
        }
    }
    
})

$('.equipe .content-equipe .close-equipe').click(function(){
        let article = $(this).parents('article');
        article.removeClass('open-equipe');
        article.find('.content-equipe h4').css("transition", "all 0.3s ease 0s");
        article.find('.content-equipe p').css("transition", "all 0.3s ease 0.2s");
        article.find('.content-equipe ol').css("transition", "all 0.3s ease 0.3s");
        closeDesc();
        function closeDesc(){
            let desc = article.find('.content-equipe');
            desc.delay(700).animate({
                width: "0%",
                padding: "0vw"
            }, 500);
        }
})



/*******************************************CALENDRIER**************************************** */

$('.calendrier ul li').click(function(){
    let classThis = $(this).attr('class');

    if( classThis === 'cl-open' ){
        $(this).css("max-height", "80px");
        $(this).removeClass('cl-open');
    }else{
        $(this).css("max-height", "1000px");
        $(this).addClass('cl-open');
    }
})



/*******************************************CONTACT**************************************** */

let valInit;
let first = true;

$(window).scroll(function(){
    let page = window.location.pathname;
    if(page.search('/contact.php') !== -1){
        let scrollToTop = $(this).scrollTop();
        let posTop = document.getElementById('textarea').offsetTop;
        //console.log(posTop);
        if(scrollToTop >= posTop - 450 && first){
            first = false;
            animTextArea();
            setTimeout(function(){
                textarea();
            }, 1500);
        }
    }//if
})

function animTextArea(){
    $('.form').animate({width: '100%'}, 500);
    $('.form form').animate({width: '100%'}, 500);
    setTimeout(function(){
        $('.form .nom').css("opacity", "1");
        setTimeout(function(){
            $('.form .email').css("opacity", "1");
        }, 300);
    }, 500);
}//animTextArea

function textarea(){
    let lieux = $('.form-img .form textarea');
    let text = 'Bonjour Seelab formation, \n\n';
    let nb_car = text.length;
    txt = new Array();

    for(let i = 0; i < nb_car; i++){
        txt.push(text[i]);
    }

    lieux.focus();
    var interval = setInterval(afficheMessage, 100);
    
        //afficheMessage();
    function afficheMessage(){
        document.getElementById("textarea").innerHTML += txt[0];
        txt.splice(0, 1);
        if(!txt[0]){
            clearInterval(interval);
            valInit = $('.form-img .form textarea').val();
            setTimeout(function(){
                $('.validation').css('opacity', '1');
                $('button[type="submit"]').css('opacity', '1');
            },300)
        }
    }//afficheMessage
    //lieux.val(text);
    //lieux.append(text);
    $('.form-img .form textarea').trigger("click");
}//textarea


$('.form-img .form textarea').keydown(envoiBtn);

function envoiBtn(){
    setTimeout(function(){
        let textarea = $('.form-img .form textarea');
        let valModif = textarea.val();


        if(valInit == valModif || valModif == ''){
            $('.form-img .form .tapez').css('top', '0');
            $('.form-img .form .envoyer').css('top', '30px');
        } else{
            $('.form-img .form .tapez').css('top', '-30px');
            $('.form-img .form .envoyer').css('top', '0');
        }
    }, 100);
}


});//doc.ready