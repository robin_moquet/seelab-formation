<?php require('header.php'); ?>

<div class="titre3 titre3BisB" style="background-image: url(sliderImg/1.jpg);">
    <div class="wrap pad-g">
        <div class="contenu">
            <h1>Une question ?<span>Besoin d'un renseignement ?</h1>
            <div class="socialBar">
                <ul>
                    <li>
                        <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/SeelabStudio" target="_blank">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<section class="bg-color">
    <div class="ateliers-text">
        <div class="wrap pad-g">
            <p>Venez vous amusez avec vos images, découvrir Photoshop ou illustrator au sein d'un studio de création graphique</p>
            <p>Juste pour se faire et s'amuser, vous pouvez choisir la formation qui vous convient le mieux :</p>
        </div>
    </div>

    <section class="ateliers wrap pad-g">
        <div class="triple-bloc">
                <article>
                    <div class="imgFor">
                    <i class="icon-Fichier-8 bis"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>
                    </div>
                    <h3>À l'année avec un<br/> cours par mois</h3>
                    <div class="bouton">
                        <a href="atelier.php">En savoir plus</a>
                    </div>
                </article>
                <article>
                    <div class="imgFor">
                    <i class="icon-Fichier-10"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span><span class="path7"></span><span class="path8"></span><span class="path9"></span><span class="path10"></span></i>
                    </div>
                    <h3>Sous forme de workshop<br/> de 3 jours intensif</h3>
                    <div class="bouton">
                        <a href="workshop.php">En savoir plus</a>
                    </div>
                </article>
                <article>
                    <div class="imgFor">
                    <i class="icon-Fichier-1"><span class="path1"></span><span class="path2"></span><span class="path3"></span></i>
                    </div>
                    <h3>Ou en cours particulier</br> à la carte</h3>
                    <div class="bouton">
                        <a href="cours-particulier.php">En savoir plus</a>
                    </div>
                </article>
        </div>
        <div class="bloc1">
            <p>Ici on mets de coté le coté académique dʼune formation institutionnelle et on vient juste pour le fun.</p>
            <p>Venez découvrir le plaisir de la création graphique sur ordinateur avec des graphistes confirmées.</p>
            <p>Nous vous apprendrons à dessiner sur illustrator, à retoucher vos images comme un photographe ou méme à faire vous même
                votre faire-part de naissance !</p>
            <p>Le maitre mot : la convivialité !</p>
        </div>
    </section>
</section>

<?php require('footer.php'); ?>