<?php require('header.php'); ?>

<div class="titre3 titre3Bis" style="background-image: url(img/financement-formation.jpg);">
    <div class="wrap pad-g">
        <div class="contenu">
            <h1>3 jours intensifs<span> sur une thématique liée à lʼimage</span></h1>
            <div class="socialBar">
                <ul>
                    <li>
                        <a href="https://www.linkedin.com/company/9485708/" target="_blank">
                            <i class="fab fa-linkedin-in"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.facebook.com/seelabstudio/" target="_blank">
                            <i class="fab fa-facebook-f"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://www.instagram.com/seelab_studio/" target="_blank">
                            <i class="fab fa-instagram"></i>
                        </a>
                    </li>
                    <li>
                        <a href="https://twitter.com/SeelabStudio" target="_blank">
                            <i class="fab fa-twitter"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div class="btn-fixe-resa">
    <a href="#">
        <img src="img/calendrier.png" alt="calendrier">
        <h3>RÉSERVER</h3>
    </a>
</div>


<div class="bg-blue">
    <div class="wrap pad-g">
        <p>Venez passer 3 demi jours au sein de notre studio pour vous familiariser avec
        lʼun de vos logiciel favoris.</p>
        <p>Nous organisons des thématique comme : je crée les visuels pour mon
        mariage, ou je fais une brochure 3 volets; ou tout autre idée que vous nous
        soumettrez !</p>

    </div>
</div>

<br/>
<br/>
<br/>

<div class="wrap pad-g">
    <h3>Quand ?</h3>
    <div class="p-20">
        <p>Selon le planning et les inscriptions.</p>
    </div>
</div>
<br/>
<br/>
<br/>

<div class="wrap pad-g">
    <h3>Comment se déroule le workshop ?</h3>
    <div class="p-20">
        <p>14h00 – Tour de table autour dʼun café ou dʼun jus de fruit. Chacun se
        présente, avec ses envies, son expérience, son niveau sur Photoshop, son
        projet.</p>
        <p>14h30 – Temps de découverte du logiciel : détourage – déplacement et
        transformation – palette des calques. Démonstration sur rétro-projecteur et
        exercices ludiques sur des exemples significatifs.</p>
        <p>16h00 – Pause</p>
        <p>16h15 – Temps de création libre sur vos photos.</p>
        <p>18h00 – Fin de la séance</p>
    </div>
</div>
<br/>
<br/>
<br/>

<div class="wrap pad-g">
    <h3>Tarif</h3>
    <div class="p-20">
        <p>XX € par demi journée</p>
        <p>Devis personnalisé sur demande.</p>
        <p>Facture sur demande.</p>
        <p>CESU accepté. Déclaré Organisme de Formation, éligible au CPF.</p>
    </div>
</div>

<section class="calendrier wrap pad-g">
    <h2>Calendrier</h2>
    <ul>
        <li>
            <header>
                <div class="cl-date">
                    <h2>14</h2>
                    <h6>Juillet</h6>
                </div>
                <div class="cl-titre">
                    <h2>Nom de l'évenement</h2>
                </div>
            </header>
            <div class="cl-content">
                <div class="cl-icon">
                    <i class="icon-Fichier-8 bis"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>
                </div>
                <div class="cl-text">
                    <h3>Détail de l'évenement</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere natus exercitationem aliquam reiciendis odit deleniti consequatur, assumenda laboriosam quia doloribus doloremque rerum soluta ipsum praesentium impedit? Tenetur repellat voluptate veritatis.</p>
                    <h3>Horaire</h3>
                    <p>Chauqe vendredi de 14H à 17H</p>
                    <h6>Nous contacter par telephone - 05 16 19 30 75</h6>
                </div>
            </div>
        </li>
        <li>
            <header>
                <div class="cl-date">
                    <h2>14</h2>
                    <h6>Juillet</h6>
                </div>
                <div class="cl-titre">
                    <h2>Nom de l'évenement</h2>
                </div>
            </header>
            <div class="cl-content">
                <div class="cl-icon">
                    <i class="icon-Fichier-8 bis"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>
                </div>
                <div class="cl-text">
                    <h3>Détail de l'évenement</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere natus exercitationem aliquam reiciendis odit deleniti consequatur, assumenda laboriosam quia doloribus doloremque rerum soluta ipsum praesentium impedit? Tenetur repellat voluptate veritatis.</p>
                    <h3>Horaire</h3>
                    <p>Chauqe vendredi de 14H à 17H</p>
                    <h6>Nous contacter par telephone - 05 16 19 30 75</h6>
                </div>
            </div>
        </li>
        <li>
            <header>
                <div class="cl-date">
                    <h2>14</h2>
                    <h6>Juillet</h6>
                </div>
                <div class="cl-titre">
                    <h2>Nom de l'évenement</h2>
                </div>
            </header>
            <div class="cl-content">
                <div class="cl-icon">
                    <i class="icon-Fichier-8 bis"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>
                </div>
                <div class="cl-text">
                    <h3>Détail de l'évenement</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere natus exercitationem aliquam reiciendis odit deleniti consequatur, assumenda laboriosam quia doloribus doloremque rerum soluta ipsum praesentium impedit? Tenetur repellat voluptate veritatis.</p>
                    <h3>Horaire</h3>
                    <p>Chauqe vendredi de 14H à 17H</p>
                    <h6>Nous contacter par telephone - 05 16 19 30 75</h6>
                </div>
            </div>
        </li>
        <li>
            <header>
                <div class="cl-date">
                    <h2>14</h2>
                    <h6>Juillet</h6>
                </div>
                <div class="cl-titre">
                    <h2>Nom de l'évenement</h2>
                </div>
            </header>
            <div class="cl-content">
                <div class="cl-icon">
                    <i class="icon-Fichier-8 bis"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>
                </div>
                <div class="cl-text">
                    <h3>Détail de l'évenement</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere natus exercitationem aliquam reiciendis odit deleniti consequatur, assumenda laboriosam quia doloribus doloremque rerum soluta ipsum praesentium impedit? Tenetur repellat voluptate veritatis.</p>
                    <h3>Horaire</h3>
                    <p>Chauqe vendredi de 14H à 17H</p>
                    <h6>Nous contacter par telephone - 05 16 19 30 75</h6>
                </div>
            </div>
        </li>
        <li>
            <header>
                <div class="cl-date">
                    <h2>14</h2>
                    <h6>Juillet</h6>
                </div>
                <div class="cl-titre">
                    <h2>Nom de l'évenement</h2>
                </div>
            </header>
            <div class="cl-content">
                <div class="cl-icon">
                    <i class="icon-Fichier-8 bis"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>
                </div>
                <div class="cl-text">
                    <h3>Détail de l'évenement</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere natus exercitationem aliquam reiciendis odit deleniti consequatur, assumenda laboriosam quia doloribus doloremque rerum soluta ipsum praesentium impedit? Tenetur repellat voluptate veritatis.</p>
                    <h3>Horaire</h3>
                    <p>Chauqe vendredi de 14H à 17H</p>
                    <h6>Nous contacter par telephone - 05 16 19 30 75</h6>
                </div>
            </div>
        </li>
        <li>
            <header>
                <div class="cl-date">
                    <h2>14</h2>
                    <h6>Juillet</h6>
                </div>
                <div class="cl-titre">
                    <h2>Nom de l'évenement</h2>
                </div>
            </header>
            <div class="cl-content">
                <div class="cl-icon">
                    <i class="icon-Fichier-8 bis"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>
                </div>
                <div class="cl-text">
                    <h3>Détail de l'évenement</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere natus exercitationem aliquam reiciendis odit deleniti consequatur, assumenda laboriosam quia doloribus doloremque rerum soluta ipsum praesentium impedit? Tenetur repellat voluptate veritatis.</p>
                    <h3>Horaire</h3>
                    <p>Chauqe vendredi de 14H à 17H</p>
                    <h6>Nous contacter par telephone - 05 16 19 30 75</h6>
                </div>
            </div>
        </li>
        <li>
            <header>
                <div class="cl-date">
                    <h2>14</h2>
                    <h6>Juillet</h6>
                </div>
                <div class="cl-titre">
                    <h2>Nom de l'évenement</h2>
                </div>
            </header>
            <div class="cl-content">
                <div class="cl-icon">
                    <i class="icon-Fichier-8 bis"><span class="path1"></span><span class="path2"></span><span class="path3"></span><span class="path4"></span><span class="path5"></span><span class="path6"></span></i>
                </div>
                <div class="cl-text">
                    <h3>Détail de l'évenement</h3>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Facere natus exercitationem aliquam reiciendis odit deleniti consequatur, assumenda laboriosam quia doloribus doloremque rerum soluta ipsum praesentium impedit? Tenetur repellat voluptate veritatis.</p>
                    <h3>Horaire</h3>
                    <p>Chauqe vendredi de 14H à 17H</p>
                    <h6>Nous contacter par telephone - 05 16 19 30 75</h6>
                </div>
            </div>
        </li>
    </ul>
</section>

<br/>
<br/>
<br/>
<br/>

<section class="lien-agence">
    <a href="contact.php#formlaire">
        <h2>Intéressé ? Contactez-nous →</h2>
    </a>
</section>




<?php require('footer.php'); ?>